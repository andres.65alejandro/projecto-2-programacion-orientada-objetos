/**********************************************************************
 * Instituto Tecnológico de Costa Rica Programación Orientada a Objetos IC-2101
 * II Semestre 2019 Profesora: Samanta Ramijan Carmiol Ejemplos Prácticos:
 * Interfaces
 * Steven Alvarado Aguilar
 * Andrés López Sanchéz
 * Fabián López Sanchéz
 **********************************************************************/
import java.util.Arrays;
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author fabia
 */
public class Destructor extends Barco {

 
  Destructor() {
      casillas=2;
      nombre ="Destructor";
      zona = new int[2][2];
      vida=2;
      Destructor.this.solicitarPosicion();
    }
    
  public void solicitarPosicion(){
     System.out.println("\n\nIngrese la coordenada X de la primera posicion de su Destructor");
     int x = sc.nextInt();
     System.out.println("X:"+x);
     
     System.out.println("Ingrese la coordenada Y de la primera posicion de su Destructor");
     int y= sc.nextInt();
     System.out.println("Y:"+y);
     
     Destructor.this.ubicacionDestructor(x, y);
     }
  public void ubicacionDestructor(int x,int y){
        
        coord1 = new int[2];
        coord1[0] = x;
        coord1[1]=y;
        
       System.out.println("Si su barco va verticalmente ingrese 1 y si va horizontalmente ingrese 2");
        int d = sc.nextInt();
        if(d == 1){
      
        coord2 = new int[2];
        coord2[0] = x;
        coord2[1]=y+1;
       
        }
      if(d ==2){
      
        coord2 = new int[2];
        coord2[0] = x+1;
        coord2[1]=y;
        
        
        }if (d !=1 && d!=2){
          System.out.println("Ingreso un numero no valido");
   
      }
        Destructor.this.arrayDeCoordenadas(coord1, coord2);
 
    }
    public void arrayDeCoordenadas(int x[],int z[]){
        int [][]ubicacion;
        ubicacion = new int [2][2];
        ubicacion[0]= coord1;
        ubicacion[1]= coord2;
        System.out.println("|Asegurese de no repetir las siguentes coordenadas!1");
        System.out.print(Arrays.deepToString(ubicacion));
        zona=ubicacion;
        
    }
}