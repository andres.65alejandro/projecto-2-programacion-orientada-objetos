/**********************************************************************
 * Instituto Tecnológico de Costa Rica Programación Orientada a Objetos IC-2101
 * II Semestre 2019 Profesora: Samanta Ramijan Carmiol Ejemplos Prácticos:
 * Interfaces
 * Steven Alvarado Aguilar
 * Andrés López Sanchéz
 * Fabián López Sanchéz
 **********************************************************************/
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
public class Jugador {
    private Tablero tableroPlayer;
    private Tablero tableroJugador;
    private int vida =21;
    private boolean turno;
    List<Barco> barcosJugador = new ArrayList<>();
    Barco asignar =new Barco();
    Scanner sc = new Scanner(System.in);
   
    public Jugador() {
        Tablero tableroJugador = new Tablero(10,10);
        tableroJugador.crearTablero();
        tableroJugador.imprimirMatriz();
        barcosJugador = asignar.listaBarcos();
        tableroJugador.cambiarCoordenadas(barcosJugador, tableroJugador);
        tableroJugador.imprimirMatriz();
        tableroPlayer =tableroJugador;
    }
  
    public Tablero getTableroJugador() {
        return tableroJugador;
       
    }

    public void setTableroJugador(Tablero tableroJugador) {
        this.tableroJugador = tableroJugador;
    }


    public boolean cambioTurno(){
        if(turno == false){
            return turno = true;
        }
        else{
            return turno = false;
        }
    }

    public void setVida(int vida) {
        this.vida = vida;
    }

    public void setTurno(boolean turno) {
        this.turno = turno;
        if(turno == false){
            turno = true;
        }
        if(turno == true){
            turno = false;
        
        }
    }

    public int getVida() {
        return vida;
    }

    public Tablero getTableroPlayer() {
        return tableroPlayer;
    }

    public void setTableroPlayer(Tablero tableroPlayer) {
        this.tableroPlayer = tableroPlayer;
    }
        public void disparos(List<Barco> yat, Tablero board,Jugador J){
        
        int[][] tableroActual = board.getMatriz();
        int X = 3;
        int F = 4;
        int H = 5;
        int t = 0 ;
        
        System.out.println("Ingrese la coordenada X de las coordenadas que quiere disparar");
        int x = sc.nextInt();
        System.out.println("X:"+x);
     
        System.out.println("Ingrese la coordenada Y de las coordenadas que quiere disparar");
        int y = sc.nextInt();
        System.out.println("Y:"+y);
        
        if (tableroActual[x][y] == 1) {
            tableroActual[x][y]=F;
            int life = yat.get(t).getVida()-1;
            yat.get(t).setVida(life);

           J.vida=J.vida-1;
            for ( yat.get(t); t>10; t++){
                int s = 0;
                if(yat.get(t).zona[s][0]==x && yat.get(t).zona[s][1]== y){

                    if(yat.get(t).vida == 0){
                        int g = 0;
                        for(yat.get(t);g>yat.get(t).casillas+1;g++){
                            int a = 0;
                            int b = 0;
                            int p=s;
                            yat.get(t).zona[p][0]=a;
                            yat.get(t).zona[p][1]=b;
                            tableroActual[a][b]=H;
                        }
                    }
                }
                
                  s++;      
            }
            
        }
        if (tableroActual[x][y] == 0) {
            tableroActual[x][y]=X;
        }
        }
            
}













