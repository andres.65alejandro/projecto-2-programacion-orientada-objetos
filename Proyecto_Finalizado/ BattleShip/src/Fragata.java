/**********************************************************************
 * Instituto Tecnológico de Costa Rica Programación Orientada a Objetos IC-2101
 * II Semestre 2019 Profesora: Samanta Ramijan Carmiol Ejemplos Prácticos:
 * Interfaces
 * Steven Alvarado Aguilar
 * Andrés López Sanchéz
 * Fabián López Sanchéz
 **********************************************************************/
import java.util.Arrays;
import java.util.Scanner;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author fabia
 */
public class Fragata extends Barco {

 
    Fragata(){
      casillas=1;
      nombre ="fragata";
      zona = new int[1][1];
      vida=1;
       Fragata.this.solicitarPosicion();
    }
 
  public void solicitarPosicion(){
     System.out.println("\n\nIngrese la coordenada X de la primera posicion de su Fragata");
     int x = sc.nextInt();
     System.out.println("X:"+x);
     
     System.out.println("Ingrese la coordenada Y de la primera posicion de su Fragata");
     int y= sc.nextInt();
     System.out.println("Y:"+y);
     
     Fragata.this.ubicacionFragatas(x, y);
     }
  public void ubicacionFragatas(int x,int y){
        
        coord1 = new int[2];
        coord1[0] = x;
        coord1[1]=y;
        
        Fragata.this.arrayDeCoordenadas(coord1);
      }
 public void arrayDeCoordenadas(int x[]){
        int [][]ubicacion;
        ubicacion = new int [1][1];
        ubicacion[0]= coord1;
        System.out.println("|Asegurese de no repetir las siguentes coordenadas!1");
        System.out.print(Arrays.deepToString(ubicacion));
        zona = ubicacion;
    }

    public int getVida() {
        return vida;
    }
    
}
