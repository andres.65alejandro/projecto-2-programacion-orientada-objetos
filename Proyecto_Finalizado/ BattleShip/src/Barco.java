/**********************************************************************
 * Instituto Tecnológico de Costa Rica Programación Orientada a Objetos IC-2101
 * II Semestre 2019 Profesora: Samanta Ramijan Carmiol Ejemplos Prácticos:
 * Interfaces
 * Steven Alvarado Aguilar
 * Andrés López Sanchéz
 * Fabián López Sanchéz
 **********************************************************************/
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author fabia
 */
public class Barco {
 public String nombre;
 public int vida;
 public int casillas;
 public int d;
 public int coord1[];
 public int coord2[];
 public int coord3[];
 public int coord4[];
 public int [][]zona;
 Scanner sc = new Scanner(System.in);

 

    public Barco() {

}
    public List<Barco> listaBarcos(){
       List<Barco> barcos = new ArrayList<>();
       
       Portaavion portavion = new Portaavion();
       Submarino submarino1 = new Submarino();
       Submarino submarino2 = new Submarino();
       Submarino submarino3 = new Submarino();
       Destructor destructor1 = new Destructor();
       Destructor destructor2 = new Destructor();
       Destructor destructor3 = new Destructor();
       Fragata fragata1 = new Fragata();
       Fragata fragata2 = new Fragata();
       
       
       barcos.add(portavion);
       barcos.add(submarino1);
       barcos.add(submarino2);
       barcos.add(submarino3);
       barcos.add(destructor1);
       barcos.add(destructor2);
       barcos.add(destructor3);
       barcos.add(fragata1);
       barcos.add(fragata2);

    return barcos;
    }

    @Override
    public String toString() {
        return "Barcos{" + "nombre=" + nombre + ", vida=" + vida + ", zona=" + Arrays.deepToString(zona) + '}';
    }

    public int getVida() {
        return vida;
    }

    public void setVida(int vida) {
        this.vida = vida;
    }
    


   
    
}