/**********************************************************************
 * Instituto Tecnológico de Costa Rica Programación Orientada a Objetos IC-2101
 * II Semestre 2019 Profesora: Samanta Ramijan Carmiol Ejemplos Prácticos:
 * Interfaces
 * Steven Alvarado Aguilar
 * Andrés López Sanchéz
 * Fabián López Sanchéz
 **********************************************************************/
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author fabia
 */
public class Portaavion extends Barco{

    

    Portaavion() {
        casillas =4;
        nombre ="portavion";
        zona = new int[4][4];
        vida =4;
        Portaavion.this.solicitarPosicionPortaavion();
    }
    
 public void solicitarPosicionPortaavion(){
     System.out.println("Ingrese la coordenada X de la primera posicion de su Portaavion");
     int x = sc.nextInt();
     System.out.println("X:"+x);
     
     System.out.println("Ingrese la coordenada Y de la primera posicion de su Portaavion");
     int y= sc.nextInt();
     System.out.println("Y:"+y);
     
     Portaavion.this.ubicacionPortaavion(x, y);
             }
    public void ubicacionPortaavion(int x,int y){
        
        coord1 = new int[2];
        coord1[0] = x;
        coord1[1]=y;
        
       System.out.println("Si su barco va verticalmente ingrese 1 y si va horizontalmente ingrese 2");
        int d = sc.nextInt();
        if(d == 1){
      
        coord2 = new int[2];
        coord2[0] = x;
        coord2[1]=y+1;
        
    
        coord3 = new int[2];
        coord3[0] = x;
        coord3[1]=y+2;
        
      
        coord4 = new int[2];
        coord4[0] = x;
        coord4[1]=y+3;
        
        }
      if(d ==2){
      
        coord2 = new int[2];
        coord2[0] = x+1;
        coord2[1]=y;
        
   
        coord3 = new int[2];
        coord3[0] = x+2;
        coord3[1]=y;
        
       
        coord4 = new int[2];
        coord4[0] = x+3;
        coord4[1]=y;
        
        }if (d !=1 && d!=2){
          System.out.println("Ingreso un numero no valido");
          Portaavion.this.ubicacionPortaavion(x, y);
      }
        Portaavion.this.arrayDeCoordenadas(coord1, coord2, coord3, coord4);
    }      
    
    public void arrayDeCoordenadas(int x[],int z[],int f[],int g[]){
        int [][]ubicacion;
        ubicacion = new int [4][4];
        ubicacion[0]= coord1;
        ubicacion[1]= coord2;
        ubicacion[2]= coord3;
        ubicacion[3]= coord4;
        System.out.println("|Asegurese de no repetir las siguentes coordenadas!1");
        System.out.print(Arrays.deepToString(ubicacion));
            zona=ubicacion;
            
      }
    public void imprimirUbicacionPortaavion(int ubicacion[][]){
        System.out.print(Arrays.deepToString(ubicacion));
    }
    
    
    
}

