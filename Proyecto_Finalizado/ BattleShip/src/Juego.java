/**********************************************************************
 * Instituto Tecnológico de Costa Rica Programación Orientada a Objetos IC-2101
 * II Semestre 2019 Profesora: Samanta Ramijan Carmiol Ejemplos Prácticos:
 * Interfaces
 * Steven Alvarado Aguilar
 * Andrés López Sanchéz
 * Fabián López Sanchéz
 **********************************************************************/
import java.util.ArrayList;
import java.util.List;


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author fabia
 */
public class Juego {
    List<Jugador> listJugadores = new ArrayList<>();
   List<Jugador>players = new ArrayList<>();

    public Juego() {
        listaJugadores();
    }
    
    
    public void listaJugadores(){
        List<Jugador> jugadores = new ArrayList<>();
        Jugador jugador1 = new Jugador();
        Jugador jugador2 = new Jugador();
        jugadores.add(jugador1);
        jugadores.add(jugador2);
         players =jugadores;
         play();
        
        
   }
    public void play(){
        Tablero tablero1 = players.get(0).getTableroPlayer();
        List<Barco> barcos1 = players.get(0).barcosJugador;
        Tablero tablero2 = players.get(1).getTableroPlayer();
        List<Barco> barcos2 = players.get(1).barcosJugador;
        Jugador jugador1 = players.get(0);
        Jugador jugador2 = players.get(1);
        
        
       while(players.get(0).getVida()> 0 && players.get(1).getVida()>0){
           tablero1.imprimirMatriz();
           System.out.println("Este es el tablero del jugador 1.");
           System.out.println("|Turno del jugador 1|");
           jugador1.disparos(barcos2, tablero2, jugador2);
           tablero2.imprimirMatriz();
           System.out.println("Este es el tablero del jugador 2.");
           System.out.println("|Turno del jugador 2|");
           jugador2.disparos(barcos1, tablero1, jugador1);
       
       }
       if(players.get(0).getVida()>players.get(1).getVida()){
           System.out.println("|El jugador 1 gano!|");
       }
       if(players.get(0).getVida()<players.get(1).getVida()){
           System.out.println("|El jugador 2 gano!|");
      }
    }
}