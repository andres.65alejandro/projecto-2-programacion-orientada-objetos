/**********************************************************************
 * Instituto Tecnológico de Costa Rica Programación Orientada a Objetos IC-2101
 * II Semestre 2019 Profesora: Samanta Ramijan Carmiol Ejemplos Prácticos:
 * Interfaces
 * Steven Alvarado Aguilar
 * Andrés López Sanchéz
 * Fabián López Sanchéz
 **********************************************************************/
import java.util.Scanner;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author fabia
 */
public class Tablero {
    int i;
    int j;
    private int matriz[][];
  Scanner sc = new Scanner(System.in);

    public Tablero(int i, int j) {
        this.i = i;
        this.j = j;
    }

    public int[][] crearTablero(){
        matriz = new int[i][j];
        int x=0;
        int y=0;
        while(x != i){
            while (y!=j){
                matriz[x][y]= 0;
                y++;
            }
            y=0;
            x++;
        }
        return matriz;
    }
    public void crearTableroEscondido(){
        matriz = new int[i][j];
        int x=0;
        int y=0;
        while(x != i){
            while (y!=j){
                matriz[x][y]= 8;
                y++;
            }
            y=0;
            x++;
        }
        
    }
    public int getI() {
        return i;
    }

    public void setI(int i) {
        this.i = i;
    }

    public int getJ() {
        return j;
    }

    public void setJ(int j) {
        this.j = j;
    }

    public int[][] getMatriz() {
        return matriz;
    }

    public void setMatriz(int[][] matriz) {
        this.matriz = matriz;
    }

   


  public void imprimirMatriz(){
      System.out.println("\n\n");
      for (int x=0; x < matriz.length; x++) {
          System.out.print("|");
          for (int y=0; y < matriz[x].length; y++) {
              System.out.print (matriz[x][y]);
              if (y!=matriz[x].length-1) System.out.print(" ");
          }
          System.out.println("|");
        }
      System.out.println(lineaNueva);
  }
  String lineaNueva = System.getProperty("line.separator");
  
   public void cambiarCoordenadas(List<Barco> ships,Tablero board){
        int I = 0;
        int A = 0;
        int[][] agua =board.getMatriz();
        
        while(I <ships.size()){

            while(A <ships.get(I).vida){
                int x=ships.get(I).zona[A][0];
                 int y=ships.get(I).zona[A][1];
            agua[x][y]=1; 
            A++;
            }
            A=0;
            I++;
        }
   }
        public void disparos(List<Barco> yat, Tablero board,Jugador J){
        
        int[][] tableroActual = board.getMatriz();
        int X = 3;
        int F = 4;
        int H = 5;
        int t = 0 ;
        
        System.out.println("Ingrese la coordenada X de las coordenadas que quiere disparar");
        int x = sc.nextInt();
        System.out.println("X:"+x);
     
        System.out.println("Ingrese la coordenada Y de las coordenadas que quiere disparar");
        int y = sc.nextInt();
        System.out.println("Y:"+y);
        
        if (tableroActual[x][y] == 1) {
            tableroActual[x][y]=F;
            for ( yat.get(t); t>10; t++){
                int s = 0;
                if(yat.get(t).zona[s][0]==x && yat.get(t).zona[s][1]== y){
                        yat.get(t).vida =yat.get(t).vida-1;
                    if(yat.get(t).vida == 0){
                        int g = 0;
                        for(yat.get(t);g>yat.get(t).casillas+1;g++){
                            int a = 0;
                            int b = 0;
                            int p=s;
                            yat.get(t).zona[p][0]=a;
                            yat.get(t).zona[p][1]=b;
                            tableroActual[a][b]=H;
                        }
                    }
                }
                
                  s++;      
            }
            
        }
        if (tableroActual[x][y] == 0) {
            tableroActual[x][y]=X;
        }
        } 

    @Override
    public String toString() {
        return "Tablero{" + "matriz=" + matriz + '}';
    }
      

     }
    

