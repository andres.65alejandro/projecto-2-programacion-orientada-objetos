import java.util.ArrayList;
import java.util.List;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @Andres y Fabian Lopez sanchez 
 */
public class Jugador {
    private String nombre;
    private Tablero tableroJugador;
    private Tablero tableroEscondido;
    private int vida ;
    private boolean turno;
    private Barco barco;

    private List<Barco> barcos= new ArrayList<>(); //Una nueva lista matriz de nombre barco.

    public Jugador() {
        this.vida = 19;
        this.turno = false;
        Tablero tableroJugador = new Tablero(10,10);
        tableroJugador.crearTablero(turno);
        Tablero tableroEscondido= new  Tablero(10,10);
        tableroEscondido.crearTableroEscondido(turno);
        tableroJugador.imprimirMatriz();
        tableroEscondido.imprimirMatriz();
        System.out.println(vida);
        System.out.println(turno);
        Jugador.this.setTurno(turno);
        
        
    }
    
    public Tablero getNombre() {
        return nombre;
    }

    public Tablero getTableroJugador() {
        return tableroJugador;
        
    }

    public Tablero getTableroEscondido() {
        return tableroEscondido;
    }

    public void setTableroJugador(Tablero tableroJugador) {
        this.tableroJugador = tableroJugador;
    }

    public void setTableroEscondido(Tablero tableroEscondido) {
        this.tableroEscondido = tableroEscondido;
    }
    
    public int ataque(){
        vida = vida -1;
       return vida;
    }
    public boolean cambioTurno(){
        if(turno == false){
            return turno = true;
        }
        else{
            return turno = false;
        }
    }

    public void setVida(int vida) {
        this.vida = vida;
    }

    public void setTurno(boolean turno) {
        this.turno = turno;
        if(turno == false){
            turno = true;
        }
        if(turno == true){
            turno = false;
        
        }
    }

    public Tablero getBarco() {
        return barco;
    }
    
}

