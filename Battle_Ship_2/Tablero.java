/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author fabian
 */
public class Tablero {
    int i;
    int j;
    private String matriz[][];

    public Tablero(int i, int j) {
        this.i = i;
        this.j = j;
    }

    public void crearTablero(boolean esVisible){
        matriz = new String[i][j];
        int x=0;
        int y=0;
        while(x != i){
            while (y!=j){
                matriz[x][y]= "o";
                y++;
            }
            y=0;
            x++;
        }
    }
    public void crearTableroEscondido(boolean esvisible){
        matriz = new String[i][j];
        int x=0;
        int y=0;
        while(x != i){
            while (y!=j){
                matriz[x][y]= "?";
                y++;
            }
            y=0;
            x++;
        }
        
    }
    public int getI() {
        return i;
    }

    public void setI(int i) {
        this.i = i;
    }

    public int getJ() {
        return j;
    }

    public void setJ(int j) {
        this.j = j;
    }

    public String[][] getMatriz() {
        return matriz;
    }

    public void setMatriz(String[][] matriz) {
        this.matriz = matriz;
    }


  public void imprimirMatriz(){
      for (int x=0; x < matriz.length; x++) {
          System.out.print("|");
          for (int y=0; y < matriz[x].length; y++) {
              System.out.print (matriz[x][y]);
              if (y!=matriz[x].length-1) System.out.print("\t");
          }
          System.out.println("|");
        }
      System.out.println(lineaNueva);
  }
  String lineaNueva = System.getProperty("line.separator");
  
  //Este método accede a la clase Jugador y Barco para poder agregar un barco a cada jugador
  public void setBarco(Jugador jugador, Barco barco){
    jugador.getBarcos().add(barco);
  }
  
  //Aca va los diferentes barcos.
  
    
}
