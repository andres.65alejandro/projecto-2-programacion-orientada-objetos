*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author fabia
 */
public class Tablero {
    int i;
    int j;
    private String matriz[][];

    public Tablero(int i, int j) {
        this.i = i;
        this.j = j;
    }
    public void crearTablero(boolean esPersonal){
        String[][] matriz = new String[this.i][this.j];
        String caracterInicial;
        if(esPersonal){
            caracterInicial = "o";
        }
        else{
            caracterInicial = "?";
        }
        for (int i = 0; i < this.i; i++) {
            for (int j = 0; j < this.j; j++) {
                matriz[i][j] = caracterInicial;
            }
        }
        this.setMatriz(matriz);
    }

    public int getI() {
        return i;
    }

    public void setI(int i) {
        this.i = i;
    }

    public int getJ() {
        return j;
    }

    public void setJ(int j) {
        this.j = j;
    }

    public String[][] getMatriz() {
        return matriz;
    }

    public void setMatriz(String[][] matriz) {
        this.matriz = matriz;
    }
    
    
}
